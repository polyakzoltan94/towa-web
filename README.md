# towa-web

> A Vue.js project

## Install Setup

``` bash
# install dependencies
npm install

# serve with hot reload at localhost:8080
npm run dev

# build for production with minification
npm run build
```

### Routes

### <localhost:port>/users
ex.: http://localhost:8080/#/users

It will return the table view of all the users that comes from backend

### <localhost:port>/users/{id}
ex.: http://localhost:8080/#/users/1

It will return an exact user with more details about him/her

### <span style="color:yellow;">IMPORTANT</span>

### **You have to modify the `src/services/api.service.js`.**

Modify the port number, that you're having in backend!

For a detailed explanation on how things work, check out the [guide](http://vuejs-templates.github.io/webpack/) and [docs for vue-loader](http://vuejs.github.io/vue-loader).

## Ideas

### Using bootstrap and (b-) like components
Bootstrap-vue has an awesome feature to work with built-in components, they're making great synergy

