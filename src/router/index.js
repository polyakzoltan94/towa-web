import Vue from 'vue'
import Router from 'vue-router'
import UserList from '@/components/UserList'
import UserView from '@/components/UserView'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      redirect: '/users'
    },
    {
      path: '/users',
      name: 'user-list',
      component: UserList
    },
    {
      path: '/users/:id',
      name: 'user-view',
      component: UserView
    }
  ]
})
